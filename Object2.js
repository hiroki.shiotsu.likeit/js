//class命令
class Member {
    constructor(firstName,lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getName(){
        return this.lastName + this.firstName;
    }
}

let m = new  Member('太郎','池田');
console.log(m.getName());

//コンストラクタはconstructorで固定

//ES2015からモジュールのサポート

z