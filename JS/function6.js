//ES 2015における引数の記法

//引数のデフォ値
//if命令がなくなりコードがシンプルに...!
function getTriangle(base = 1, height = 1){ 
    return base * heigh / 2;
}

console.log(getTriangle(5));

function multi(a, b = a){
    return a * b;
}

console.log(multi(50));
console.log(multi(5,10));

//ほかの引数をデフォ値にする場合自身より前に定義されたものだけ参照できる。


//注意点
//1.デフォルト値が適用される場合、されない場合

function  getTriangle(base = 1, height = 1){
    return base * height /2;
}

console.log(getTriangle(5,null)); //結果　0

/*
第２引数のデフォルト値が適用されず　5 * null /2となり結果が０になる
　undefinedで引数に渡した場合はデフォ値が適用される。
*/

//2.デフォルト値を持つ仮引数は、引数リストの末尾に。

/*

function getTriangle(base = 1,height){...}
console.log(getTriangle(10)); ← 10 * undefined /2 = Nan になってしまう

*/

//このような挙動は分かりづらいバグの原因になるので　後方にデフォ値のある引数を持っていくよう意図していくべき。


//必須の引数を宣言する

function show(x,y = 1){
    console.log(x);
    console.log(y);
}

show();//undefined,1

function required(){
    throw new Error('引数が不足しています');
}

function hoge(value = required()){
    return value;
}

hoge();

//可変長引数の関数を定義する

//ES2015では、仮引数の前に「...」ピリオド３個を付与することで　可変長引数となる

function sum(...nums){
    let result = 0;
    for(let num of nums){
        if(typeof num !== 'number'){
            throw new Error('指定値が数値ではありません:' + num);
        }
        result += num;
    }
    return result;
}

try {
    console.log(sum(1,3,5,7,9));
}catch(e){
    window.alert(e.message);
}

//新構文を使うメリット

/*
1.関数が可変長引数を受け取ることがわかりやすい

2.すべての配列操作が可能

*/

//...演算子による引数の展開
//...演算子は実引数を利用することで、配列（for ofで処理できるオブジェクト)を個々の値に展開できる

console.log(Math.max(15,-2,78,1));
console.log(Math.max([15,-2,78,1]));

console.log(Math.max.apply(null,[15,-2,78,1]));

//名前付き引数でコードを読みやすくする

function getTriangle({base = 1,height = 1}){
    return base * height/2;
}

console.log(getTriangle({base:5,height:4}));

//2015前
function getTriangle(args){
    if(args.base === undefined){ args.base = 1; }
    if(args.height === undefined){ args.height = 1; }
    return args.base * args.height / 2;
}

console.log(getTriangle({base:50,height:500}))

//{プロパティ名　= デフォ値...}で仮引数を宣言できるように・・・！

function show({name}){
    console.log(name);
};

let member = {
    mid: 'Y0001',
    name: '山田太郎',
    address: 't_yamada@example.com'
}

show(member);