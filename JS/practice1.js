//Stringオブジェクトのメンバ

var str1 = 'にわにはにわにわとりがいる';

console.log(str1.indexOf('にわ'));
console.log(str1.lastIndexOf('にわ'));
console.log(str1.indexOf('にわ',3));
console.log(str1.lastIndexOf('わ',5));
console.log(str1.indexOf('ガーデン'));
console.log(str1.startsWith('にわ'));
console.log(str1.endsWith('にわ'));

var str2 ='Wingsプロジェクト';


console.log(str2.charAt(4));
console.log(str2.slice(5,8));
console.log(str2.substring(5,8));
console.log(str2.substring(7,3));//Start>endの場合数値が逆になって実行される sliceの場合空白が返される。


//start endに負の値を指定した場合
console.log(str2.substring(5,-2));//負数は0としてみなされる。
console.log(str2.slice(5,-2));//後方からの文字数として認識される

var msg = '叱る';
console.log(msg.length); //3が返される '叱'がUnicodeによって4バイトで認識されるため、2文字としてカウントされてしまう→ 帰ってきた値何故か2文字だった・・・

var len = msg.length;
var num = msg.split(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g).length - 1;
console.log(msg.length - num);