//関数呼び出しと戻り値

//複数の戻り値を個別の変数に代入する。

/*
原則return命令で「return x,y:」のように複数の値は返せない
この場合配列/オブジェクトとして値を一つにまとめた上で返す必要がある。
*/

function getMaxMin(...nums){
    return[Math.max(...nums),Math.min(...nums)];
}

let result = getMaxMin(10,35,-5,78,0)
console.log(result);

let[max,min] = getMaxMin(10,35,-5,78,0);
console.log(max);
console.log(min);

//片方だけを取得したい場合
let[,min] = getMaxMin(10,55,90,-82,100000,0);





//関数自身を再帰的に呼び出す -再帰関数-

/*
再帰関数とは・・・ある関数が自分自身を呼び出すこと

利用することで・・・階乗計算のように同種の手続きを何度も呼び出すような処理をコンパクトに出来る

*/

function factorial(n){
    if(n != 0){return n * factorial(n-1);}
    return 1;
}

console.log(factorial(10));

//関数の引数も関数　-高階関数-

/*
高階関数とは・・・関数を引数、戻り値として扱う関数。
*/

function arrayWalk(data, f){
    for(var key in data){
        f(data[key],key);
    }
}

function showElement(value, key){
    console.log(key + ' : ' + value);
}

var ary = [1,2,4,8,16];
arrayWalk(ary,showElement);

//関数の中で呼び出される関数をコールバック関数という

//使い捨て関数での書き換え

function arrayWalk(data, f){
    for(var key in data){
        f(data[key],key);
    }
}

var ary = [1,2,4,8,16];
arrayWalk(
    ary,
    function(value,key){
        console.log(key + ' : ' + value);
    }
    );

//関数リテラルを利用することで、意図せぬ名前の重複を防げる。
