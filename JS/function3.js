//関数リテラルでの表現
/*
関数はデータ型の一種なので、文字列や数値と同じく、
リテラルとして表現できるし、関数リテラルを変数に代入したり、ある関数の引数として渡したり、
はたまた、戻り値として関数を返すことすら可能である。
故に、柔軟なコーディングが可能
*/

var getTriangle = function(base,height){
    return base * height/2;
}

console.log('三角形の面積は' + getTriangle(5,2));


//関数リテラル　→　[function(base,height{...})]と名前のない関数を定義した上で変数getTriangkeに格納している
//匿名関数や無名関数と呼ばれる。