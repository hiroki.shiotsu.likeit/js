//引数の様々な記法

function showMessage(value){
    console.log(value);
}

showMessage();
showMessage('山田');
showMessage('山田','鈴木');

//JSでは与える引数の数が、関数側で要求する数と違う場合、これをチェックしない。故に上の３つはどれもエラーにならない。

//引数の管理　aruguments　オブジェクト（関数配下のみ扱える）

//args.check関数

function showMessage(valiue){
    if(arguments.length !== 1){
        throw new Error('引数の数が間違っています：' + arguments.length);
    }

    console.log(value);

}

try{
    showMessage('山田','川端');
}catch(e){
    window.alert(e.message);
}

//引数のデフォルト値を設定する必要がある。
//省略できるのは後ろの引数のみ。
//引数が省略可能であることをわかりやすく示すために、接頭辞o_(optionの意味)をつけるとコードが読みやすくなる。

function getTriangle(o_base,o_height){
    if(o_base === undefined){o_base = 1;}
    if(o_height == undefined){o_height = 1;}
    return o_base * o_height / 2;
}

console.log(getTriangle(50));

//可変長引数の関数を定義する

function sum(){
    var result = 0;

    for(var i = 0,len = arguments.length; i< len; i++){
        var tmp = arguments[i];
        if(typeof tmp !== 'number'){
            throw new Error('引数が数値ではありません：' + tmp);
        }
        result += tmp;
    }
    return result;
}

try{
    console.log(sum(1,3,5,7,9));
}catch(e){
    window.alert(e.massage);
}

//明示的に宣言された引数と可変長引数を混在させる

function printf(format){
    for(var i = 0,len = arguments.length; i < len; i++){
        var pattern = new RegExp('\\{' + (i-1) + '\\}', 'g');
        format = format.replace(pattern,arguments[i]);
    }
    console.log(format);
}

printf('こんにちは、{0}さん。私は{1}です。','掛谷','山田');

//名前付き引数でコードを読みやすくする

getTriangle({base:5,height:4})
//メリット
/*
・引数が多くなっても、コードの意味がわかりやすい
・省略可能な引数をスマートに
・引数の順番を自由に変更できる。
*/

function getTriangle(args){
    if(args.base === undefined){ args.base = 1; }
    if(args.height === undefined){ args.height = 1; }
    return args.base * args.height / 2;
}

console.log(getTriangle({base:50,height:500}))