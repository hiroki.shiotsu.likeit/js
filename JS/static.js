 //静的プロパティ/静的メソッドを定義する場合はプロパティを利用できない。

 var Area = function(){}; //コンストラクター

 Area.version = '1.0';//静的プロパティの定義

 Area.triangle =  function(base,height){
     return base * height /2;
 };

 Area.diamond = function(width,height){
     return width * height/2;
 }

 console.log('Areaクラスのバージョン ： ' + Area.version);
 console.log('三角形の面積 : ' + Area.triangle(5,3));

 var a = new Area();
 console.log('菱形の面積：'　+ a.diamond(5,3));

 //最後にエラー　静的メンバーはあくまでAreaという関数おぶじぇに追加されたメンバであり、Areaの生成するインスタンスに追加される。

/*
静的メンバーを利用するときの注意点

1.静的プロパティは基本読み取り専用の用途で。
2.静的メンバの中でthisは使えない。

*/