//構文
/*
funcition 関数名(引数,...){
    ...処理...
    return 戻り値;
}

注意する点
・識別子の条件を満たす必要がある
・「その関数がどのような処理を担っているのか」がすぐわかる命名をする
・[showMessage]のように動詞+名詞が一般的
*/

function getTriangle(base,height){
    return base * height /2 ;
}

console.log('三角形の面積：' + getTriangle(5,2));