var Member = function(firstName,lastName){
    this.firstName = firstName;
    this.lastName = lastName;
};

var mem = new Member('比呂樹','塩津');
mem.getName = function(){
    return this.lastName　+ ' ' +  this.firstName;
}

console.log(mem.getName());

var mem2 = new Member('ルズビミンダ','塩津');
console.log(mem2.getName());

//同一のクラスを元に生成されたインスタンスであっても、それぞれの持つメンバーが同一であるとは限らない

Obbject.seal(this);
//でプロパティの追加消去を防げる

var data = 'Global data';
var obj1 = {data: 'obj data'};
var obj2 = {data: 'obj2 data'};

function hoge(){
    console.log(this.data);
}

hoge.call(null);
hoge.call(obj1);
hoge.call(obj2);


//配列ライクなオブジェクトを配列に変換する
function hoge(){
    var args = Array.prototype.slice.call(arguments);
    console.log(args.join('/'));
}

hoge('Angular','React','Backbone');



//コンストラクターの強制的な呼び出し
var Member = function(firstName,lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
};

var m = Member('五右衛門','田中');
console.log(m);
console.log(firstName);
console.log(m.firstName);

//この場合Memberオブジェクトが生成されず代わりにグローバル変数が生成される。　以下のような仕掛けを施すと安全

var Member = function(firstName,lastName){
    if(!(this instanceof Member)){
        return new Member(firstName.lastName);
    }
    this.firstName = firstName;
    this.lastName = lastName;
}

//コンストラクターの問題点とプロトタイプ　
//コンストラクターによるメッソ度の追加・・・・"メソッドの数に比例し、無駄なメモリを消費する"という問題点がある。
//すべてのメソッドをインスタンス化のためにコピーするのは無駄！

//javascriptではオブジェクトにメンバーを追加するために prototypeというプロパティが用意されている

var Member = function(firstName, lastName){
    this.firstName = firstName;
    this.lastName = lastName;
};

Member.prototype.getName = function(){
    return this.lastName + ' ' + this.firstName;
};

var mem = new Member('炭治郎','竈門');
console.log(mem.getName());

//プロトタイプオブジェクトを利用するために必要な２つの利点

/*
1.メモリの使用量を節減できる。
プロトタイプオブジェクトの内容はインスタンスにコピーされず暗黙的に参照される。
検索の手順
        ・インスタンス側に要求されたメンバーが存在しないか確認
        ・存在しない場合は、暗黙の参照をたどってプロトタイプオブジェクトを検索


2.メンバーの追加や変更をインスタンスがリアルタイムに認識できる
*/

//挙動の確認
var Member = function(firstName,lastName){
    this.firstName = firstName;
    this.lastName = lastName;
}

var mem = new Member('純一','加藤');

Member.prototype.getName = function(){
    return this.lastName + ' ' + this.firstName;
};

console.log(mem.getName());

//プロパティの宣言　＝＞　コンストラクタで　メソッドの宣言　＝＞　プロトタイプで
//インスタンス側でのメンバの追加や削除は、プロトタイプオブジェクトに影響を及ぼさない。

//逐一 Member.prototype~~~とやったらコードが助長になるので・・・

var Member = function(firstName,lastName){
    this.firstName = firstName;
    this.lastName = lastName;
};

Member.prototype ={
    getName : function(){
        return this.lastName + ' ' + this.firstName;
    },
    toString : function(){
        return this.lastName + this.firstName;
    }
};

/*
このようにオブジェクトリテラルを利用することで
・Memberprototype~のような記述を最低限に
・結果、オブジェクト名に変更があっ場合にも影響箇所を限定できる
・同一オブジェクトのメンバー定義が一つのブロックに収められるため、コードの可読性が向上する。
 */


 //prototypeチェーン

 var Animal = function(){};

 Animal.prototype = {
     walk : function(){
         console.log('トコトコ...');
     }
 };

 var Dog = function(){
     Animall.call(this);
 };

 Dog.prototype = new Animal();
 Dog.prototype.bark = function(){
     console.log('ワンワン！');
 }


 var d = new Dog();
 d.walk();
 d.bark();

 //クロージャを利用してプライベートメンバーを作る。

 function Triangle(){
     var _base;
     var _height;

     //メソッドの定義
     var _checkArgs = function(val){
         return(typeof val === 'number' && val > 0);
     }


     this.setBase = function(base){
         if(_checkArgs(base)){_base = base;}
     }
     this.getBase = function(){return _base;}

     this.setHeight = function(height){
         if(_checkArgs(height)){ _height = height;}
     }
     this.getHeight = function(){return _height;}


    }
     Triangle.prototype.getArea = function(){
         return this.getBase() * this.getHeight()/2;
     }

     var t = new Triangle();
     t._base = 10;
     t._height = 2;
     console.log('三角形の面積：' + t.getArea());

     t.setBase(10);
     t.setHeight(2);
     console.log(t.getHeight());

     //特権メソッドの使い方（レガシーブラウザで使う）

     //Object.defineProperty での実装

     function Triangle(){
         var _base;
         var _height;

         Object.defineProperty(
             this,
             'base',
             {
                 get: function(){
                     return _base;
                 },
                 set: function(base){
                     if(typeof base === 'number' && base > 0){
                         _base = base;
                     }
                 }
             }
         );

         Object.defineProperty(
             this,
             'height',
             {
                 get: function(){
                     return _height;
                 },
                 set: function(height){
                     if(typeof height === 'number' && height > 0){
                         _height = height;
                     }
                 }
             }
         );
     };

     Triangle.prototype.getArea - function(){
         return this.base * this.height/2;
     };

     var t = new Triangle();
     t.base = 10;
     t.height = 2;
     console.log('三角形の底辺' + t.base)
