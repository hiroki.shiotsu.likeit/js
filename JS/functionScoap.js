//スコープ
/*
スコープとは・・・「変数がスクリプトの中のどの場所から参照できるか」を決める概念
JavaScriptのスコープは、２つに分類される

・スクリプト全体から参照可能グローバルスコープ　=> グローバル変数（関数の外で宣言した変数）
・定義された関数の中のみ参照できるローカルスコープ =>　ローカル変数　(関数の中で宣言した変数)

ES2015からブロックスコープという概念も追加されている
*/

var scope = 'Global Variable';

function getValue() {
    var scope = 'Local Variable'
    return scope;
}

console.log(scope);
console.log(getValue());

//同じ変数名でもスコープが異なるので別物として認識されている。

//変数宣言に varが必要な理由

scope = 'Global Variable';

function getValue() {
    scope = 'Local Variable';
    return scope;
}


console.log(getValue());
console.log(scope);

//var命令を使わずに宣言された変数はすべてグローバル変数とみなされ、上のコードではどちらもlocal variableが返される。

var scope = 'Global Variable';

function getValue() {
    console.log(scope); //この時点でローカル変数scopeの領域が確保されており、undefinedと値が返される。
    var scope = 'Local Variable'
    return scope;
}

//バグの原因になるので極力ローカル変数は関数の先頭で宣言するのが好ましい。

//仮引数のスコープ--基本型の参照型に注意--
//基本型
var value = 10;

function decrementValue(value){
    value--;
    return value;
}

console.log(decrementValue(100));//99
console.log(value);//10,グローバル変数なので影響を受けない。

//参照型

var value = [1,2,4,8,16];//1
function deleteElement(value){//2
    value.pop();
    return value;
}

console.log(deleteElement(value));
console.log(value);

//1と2は変数として別物だが、結果的に参照しているメモリの場所が等しいため、グローバル変数にも影響を与える。

//ブロックスコープ
if(true){
    var i = 5;
}

console.log(i); //javaではエラーが出るがjavascriptだと有効になり続けるので５が出力される

//変数の意図せぬ競合を防ぐため擬似的に再現が可能

(function(){
    var i = 5;
    console.log(i);
}).call(this);

console.log(i); //変数iはスコープ外なのでエラー

//let命令ではブロックスコープに対応した。

if(true){
    let i = 5;
}

console.log(i); //エラー

//関数リテラル、Functionコンストラクターにおける　スコープの違い

var scope = 'Grobal variable';

function checkScope(){
var scope = 'Local Variavle';

var f_lit = function(){return scope;};
console.log(f_lit());

var f_con = new Function('return scope;');
console.log(f_con());

}

//functionコンストラクター配下の変数はその宣言場所に関わらず常にグローバルスコープとみなされる。