//アロー関数 ES2015

/*
アロー関数を利用することで、関数リテラルよりシンプルに記述が可能
*/

//getTriangle関数

let getTriangle = (base , height) =>{
    return base * height /2 ;
};

console.log('三角形の面積：' + getTriangle(5,2));

//(引数...) => {...関数の本体...}     =>　←これがアロー

//本体が一文の場合は更に省略が可能

let getTriangle = (base, height) => base * height /2 ;

//引数が１個の場合は引数のカッコも省略が可能
let getCircle = radius => radius * radius * Math.PI;

//引数がない場合はカッコの省略が出来ない
let show = () => console.log('こんにちは世界');

//注意点
/*
1.returnの直後に改行しない（throw ++ continue/breakも同様)
改行をセミコロンとして解釈するため

2.function命令は静的な構造を宣言する。
呼び出し元より後に書いても、コンパイル時にあらかじめ関数が登録されるのでエラーにならない　対例　4.
<script>ブロックは前に書く必要がある

3.関数はデータ型の一種である

4.関数リテラル/functionコンストラクターは実行時に評価される。
実行時に評価されるので、呼び出し元よりも先に記述する必要がある。

*/