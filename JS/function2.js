//コンストラクター経由の関数定義

var getQuadrangle　= new Function('base','height','return 1 * base * height;')
console.log('四角形の面積：' + getQuadrangle(5,5));


//あえてコンストラクター経由で使うメリットはないが・・・

var param = 'height,width';
var formula = 'return height * width/2 ;';
var diamond = new Function(param,formula);
console.log('菱形の面積：'　 + diamond(5,2));

//コンストラクターからであれば因数や関数本体を文字列として定義できる
//eval関数と同じで乱用するべきではない。
//while/for文　頻繁に呼び出される関数では使わない。