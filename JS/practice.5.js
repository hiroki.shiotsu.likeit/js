//array操作
var ary1 =['Sato','Takae','Osada','Hio','Saitoh','Sato'];
var ary2 = ['Yabuki','Aoki','Moriyama','Yamada'];

console.log(ary1.length); //配列のサイズ 6
console.log(Array.isArray(ary1)); //配列かどうか true
console.log(ary1.toString());//要素,要素,...を文字列に変換 Sato,Takae.....
console.log(ary1.indexOf('Sato')); //何番目か・・・0
console.log(ary1.lastIndexOf('Sato'));//指定した要素が合致する最後の番号 5
console.log(ary1.join('/'));//配列の区切りに指定した文字を挿入 Sato/Takae/...
console.log(ary1.slice(1));//start+n番目の要素を切り抜き
console.log(ary1.slice(1,2));
console.log(ary1.splice(1,2,'Kakeya','Yamaguchi'));//置き換え対象の要素を取得しつつ裏で置き換え
console.log(ary1.copyWithin(1,3,5));//Start+1~end番目の要素をtarget+1番目からの位置にコピー

console.log(ary2.fill('Suzuki',1,3));//2~3番目の要素をSuzukiで置換
console.log(ary1.pop());//末尾の文字を消去
console.log(ary1.push('kondo')); //y要素を末尾に追加し、追加後の配列の数が返される、
console.log(ary1.shift('')); //先頭を消去し、同上
console.log(ary1.unshift('ozawa'));//先頭に追加

//ソート
console.log(ary1.reverse()); //逆から表示
console.log(ary1.sort());//要素を昇順に表示
