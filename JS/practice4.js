//Mathオブジェクト

console.log(Math.abs(-100));//絶対値 100
console.log(Math.clz32(1));//31 32bitバイナリにおいて前方にある0ビットの個数
console.log(Math.min(20,40,60));// 最小値 20
console.log(Math.max(20,40,60));// 最大値 60
console.log(Math.pow(5,3));//べき乗 5*5*5 125
console.log(Math.random()); //0~1未満の乱数
console.log(Math.sign(-100));//整数1 負数-1 0は0
console.log(Math.ceil(1234.56));//小数点以下の切り上げ 1235
console.log(Math.ceil(-1234.56));//-1234
console.log(Math.floor(1234.56));//切り捨て1234
console.log(Math.floor(-1234.56));//-1235
console.log(Math.round(1234.56)); //四捨五入1235
console.log(Math.round(-1234.56));//-1235
console.log(Math.trunc(1234.56));//整数部分の取得 1234
console.log(Math.trunc(-1234.56));//1234
console.log(Math.sqrt(81));//9 平方根
console.log(Math.cbrt(81));//立方根
console.log(Math.hypot(3,4));//引数の2乗和の平方値 9+16 =√25 = 5
console.log(Math.cos(1));//コサイン0.5403023058681398
console.log(Math.sin(1));//sin
console.log(Math.tan(1));//tan
console.log(Math.atan2(1,3));//アークタンジェント
console.log(Math.log(10));//自然対数
console.log(Math.exp(3)); //eの累乗
console.log(Math.expm1(1));//enum-1

//すべて静的メソッド　