//Numberオブジェクトについて

console.log(Number.NaN === Number.NaN); //すべての数値と等しくない

console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MAX_SAFE_INTEGER + 1);
console.log(Number.MAX_SAFE_INTEGER + 2);//不正 安全に表せる最大の整数値

var num1 = 255;
console.log(num1.toString(16));
console.log(num1.toString(8));

var num2 = 123.45678;
console.log(num2.toExponential(2));
console.log(num2.toFixed(3));
console.log(num2.toFixed(7));//小数点以下を
console.log(num2.toPrecision(10));//整数も含めた全体桁数を。
console.log(num2.toPrecision(6));

var n = '123xxx';
console.log(Number(n));
console.log(Number.parseFloat(n));
console.log(Number.parseInt(n)); //先頭から連続した数値のみ解析できる

var d = new Date();
console.log(Number(d)); //Number関数のみ経過ミリ秒に換算した値を返す。
console.log(Number.parseFloat(d));
console.log(Number.parseInt(d));

var h = '0x10';
console.log(Number(h));
console.log(Number.parseFloat(h));
console.log(Number.parseInt(h)); //parseIntとNumberは16進数とみなす

var b = '0b11';
console.log(Number(b)); //8進数はNumberのみでしか解析ができない
console.log(Number.parseFloat(b));
console.log(Number.parseInt(b)); //

var e = '1.01e+2';
console.log(Number(e));
console.log(Number.parseFloat(e));
console.log(Number.parseInt(e)); //NumberとparseFloatは正しく解析するが、parseIntはe+2をサプレスし、小数点以下を切り捨てる。

console.log(typeof(123 + '')); //+演算子は与えられたオペランドのいずれかが文字列である場合もう片方も自動的に文字列に変換する。
console.log(typeof('123' - 0));//- 演算子は与えられた文字列のいずれかが数値である場合もう片方も数値に変換し、減算する。

var num=123;
console.log(!num); //false
console.log(!!num); //true