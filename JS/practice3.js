//nsymbolオブジェクトについて
//symbol（モノ名前）を作成するための型 文字列ではない

let sym1 = Symbol('sym');
let sym2 = Symbol('sym');

console.log(typeof sym1);
console.log(sym1.toString());
console.log(sym1 === sym2);//false 暗黙的な型変換はできないが boolean型には変換可能

//典型的な利用例
const MONDAY = 0;
const TUESDAY = 1;
const WEDNESDAY = 2;
const THURSDAY =3;
const FRYDAY = 4;
const SATURDAY = 5;
const SUNDAY = 6;
//これだと同じ数値がかぶったときにバグの混入する原因になる。
const MONDAY = Symbol();
const TUESDAY = Symbol(); //こうすれば同名であっても一意である。
